#include "request_handler_dispatcher_round_robin.h"
#include "factory.h"

REGISTER_CLASS( RequestHandler, RequestHandlerDispatcherRoundRobin )

//TODO fill in
RequestHandlerDispatcherRoundRobin::RequestHandlerDispatcherRoundRobin ( Json::Value &config ) :
                                    RequestHandlerDispatcher ( config ) {

    indexOfCurrentJob = 0;

}

RequestHandlerDispatcherRoundRobin::~RequestHandlerDispatcherRoundRobin ( ) {


}

unsigned int RequestHandlerDispatcherRoundRobin::selectRequestHandler ( const Request *req,
                                                                        const std::vector<RequestHandler *> &reqHandlers ) {

    unsigned int numberOfQueues = static_cast<unsigned int>(reqHandlers.size());

    indexOfCurrentJob++;
    return indexOfCurrentJob % numberOfQueues;

}