#include "arrival_process_dist.h"
#include "distribution.h"
#include "time_helper.h"
#include "factory.h"

REGISTER_CLASS( ArrivalProcess, ArrivalProcessDist )

//TODO fill in

ArrivalProcessDist::ArrivalProcessDist ( Json::Value &config )
        : ArrivalProcess ( config ),
          arrivalDistribution ( Factory<Distribution>::create ( config[ "dist" ] )) {
}

ArrivalProcessDist::~ArrivalProcessDist ( ) {

}

uint64_t ArrivalProcessDist::nextArrival ( uint64_t currentTime ) {
    //what should I do here?
    //add the current time with a value from the distribution and add that
    double interArrivalTimeInDouble = arrivalDistribution->nextRand ( );
    uint64_t nextArrivalTime = currentTime + convertSecondsToTime ( interArrivalTimeInDouble );


    return nextArrivalTime;
}
