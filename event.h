#ifndef EVENT_H
#define EVENT_H

#include <cstdint>
#include "listener.h"


class Request;

class RequestStream;

class RequestHandler;

// Arrival event data
struct ArrivalEventData {
    //TODO fill in
    //request_stream pointer
    RequestStream *arrivalRequestStream;

    //request pointer
    Request *arrivalRequest;
};

// Completion event data
struct CompletionEventData {
    //TODO fill in
    //change this accordingly
    RequestHandler *completionRequestHandler;

    Request *completionRequest;


};

// Union of all event data-types
union EventData {
    ArrivalEventData arrivalEvent;
    CompletionEventData completionEvent;
};

// Simulation event datatype
struct Event {
    uint64_t timestamp; // time (nanoseconds) at which callback is invoked
    void (*callback) ( const Event &e ); // callback to invoke
    EventData data; // event data

    // Event timestamp comparator for simulator
    bool operator< ( const Event &e ) const {
        //TODO fill in
        //this is a placeholder
        if ( timestamp < e.timestamp ) {
            return true;
        }
        return false;
    }
};

#endif /* EVENT_H */
