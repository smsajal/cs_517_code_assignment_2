#include "distribution_det.h"
#include "factory.h"

#include <iostream>

REGISTER_CLASS(Distribution, DistributionDet)

//TODO fill in
DistributionDet::DistributionDet(Json::Value &config) : Distribution(config) {


    distributionParameter = config["val"].asDouble();

}

DistributionDet::~DistributionDet() {


}

double DistributionDet::nextRand() {

    //std::cout<<"Inside DistributionDet::nextRand...\n";

    return distributionParameter;
}
