#include <iostream>
#include "stats_throughput.h"
#include "time_helper.h"
#include "factory.h"
#include <typeinfo>


REGISTER_CLASS( Stats, StatsThroughput )

//TODO fill in

StatsThroughput::StatsThroughput ( Json::Value &config ) : Stats ( config ) {

    resetStats ( );
}

StatsThroughput::~StatsThroughput ( ) {


}

void StatsThroughput::printStats ( uint64_t lastIntervalBegin, uint64_t lastIntervalEnd ) {

    std::cout << "t " << lastIntervalBegin
              << " X " << getThroughput ( lastIntervalEnd - lastIntervalBegin )
              << std::endl;
}

Json::Value StatsThroughput::jsonStats ( uint64_t lastIntervalBegin, uint64_t lastIntervalEnd ) {

    Json::Value results;
    results[ "t" ] = lastIntervalBegin;
    results[ "X" ] = getThroughput ( lastIntervalEnd - lastIntervalBegin );
    return results;
}

double StatsThroughput::getThroughput ( uint64_t intervalTime ) {

    //check about the casting....
    if ( convertTimeToSeconds ( intervalTime ) == 0 ) {
//        std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n";
//        std::cout << "stats_throughput.convertTimeToSeconds (intervalTime)==0\n";
//        std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n";
        return 0.0;
    }

    return ((1.0 * count) / convertTimeToSeconds ( intervalTime ));

}

void StatsThroughput::resetStats ( ) {

    //this needs to get checked....
    count = 0;

    return;
}

void StatsThroughput::notifyEnd ( Request *req ) {

    if ( checkOutputStats ( )) {
        resetStats ( );
    }

    count++;
}