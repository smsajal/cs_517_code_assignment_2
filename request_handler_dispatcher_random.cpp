#include "request_handler_dispatcher_random.h"
#include "random_helper.h"
#include "factory.h"

REGISTER_CLASS( RequestHandler, RequestHandlerDispatcherRandom )

//TODO fill in
RequestHandlerDispatcherRandom::RequestHandlerDispatcherRandom ( Json::Value &config ) : RequestHandlerDispatcher (
        config ) {


}

RequestHandlerDispatcherRandom::~RequestHandlerDispatcherRandom ( ) {

}

unsigned int RequestHandlerDispatcherRandom::selectRequestHandler ( const Request *req,
                                                                    const std::vector<RequestHandler *> &reqHandlers ) {

    double randomDouble = uniform01 ( );
    unsigned int indexToBeReturned = ( unsigned int ) (randomDouble * reqHandlers.size ( ));

    return indexToBeReturned;


}