#!/bin/bash

cd ..
make

./simulator tests/request_handler_queueing_network_initial_probability_test.json
./simulator tests/request_handler_queueing_network_loop_test.json
./simulator tests/request_handler_queueing_network_many_links_test.json
./simulator tests/request_handler_queueing_network_transition_probability_test.json

cd tests/
