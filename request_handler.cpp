#include "request_handler.h"
#include "request.h"
#include "event.h"
#include "simulator.h"

#include <stdio.h>

// Add a completion event to the event queue at the given time for a given request
simulator::EventReference RequestHandler::addCompletionEvent ( uint64_t timestamp, Request *req ) {
    //TODO fill in
    struct Event completionEvent;
    //printf("inside RequestHandler::addCompletionEvent...\n");

    //set the timestamp
    //get the completion time for timestamp
    completionEvent.timestamp = timestamp; // is this the right thing to do?

    //set callback to completionCallback
    completionEvent.callback = &completionCallback;

    //set the requestStream and request
    completionEvent.data.completionEvent.completionRequest = req;
    completionEvent.data.completionEvent.completionRequestHandler = this;

    Event &eventReference = completionEvent;
    simulator::EventReference completionEventReference = simulator::schedule ( eventReference );

    return completionEventReference;
}

// Completion callback function
void completionCallback ( const Event &e ) {
    //TODO fill in
    //printf("inside completionCallback...\n");
    //call notify end
    e.data.completionEvent.completionRequestHandler->notifyEnd ( e.data.completionEvent.completionRequest );
    return;

}
