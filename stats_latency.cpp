#include <iostream>
#include "stats_latency.h"
#include "time_helper.h"
#include "factory.h"

REGISTER_CLASS( Stats, StatsLatency )

//TODO fill in

StatsLatency::StatsLatency ( Json::Value &config ) : Stats ( config ) {

    resetStats ( );

}

StatsLatency::~StatsLatency ( ) {

}

void StatsLatency::printStats ( uint64_t lastIntervalBegin, uint64_t lastIntervalEnd ) {

    std::cout << "t " << lastIntervalBegin
              << " E[T] " << getMeanLatency ( )
              << std::endl;
}

Json::Value StatsLatency::jsonStats ( uint64_t lastIntervalBegin, uint64_t lastIntervalEnd ) {

    Json::Value results;
    results[ "t" ] = lastIntervalBegin;
    results[ "E[T]" ] = getMeanLatency ( );
    return results;
}

double StatsLatency::getMeanLatency ( ) {

    //casting maybe needed to convert nanosecond to second
    if ( count == 0 ) {
//        std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@\n";
//        std::cout << "stats_latency.count==0\n";
//        std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@\n";
        return 0;
    }
    return convertTimeToSeconds ( latencySum / count );


}

void StatsLatency::resetStats ( ) {
    //both the vars are uint64_t
    count = 0;
    latencySum = 0;

}

void StatsLatency::notifyEnd ( Request *req ) {

    if ( checkOutputStats ( )) {
        resetStats ( );
    }

    count++;
    latencySum += req->getElapsedTime ( );

}