#include "distribution_sum.h"
#include "factory.h"

#include <iostream>

REGISTER_CLASS( Distribution, DistributionSum )

//TODO fill in
DistributionSum::DistributionSum ( Json::Value &config ) :
        Distribution ( config ),
        _config ( config ) {

    init ( );
}

DistributionSum::~DistributionSum ( ) {


}

void DistributionSum::init ( ) {

    Json::Value &distributions = _config[ "dists" ];
    for ( unsigned int i = 0; i < distributions.size ( ); ++i ) {
        _distributions.push_back ( Factory<Distribution>::create ( distributions[ i ] ));
    }
}

double DistributionSum::nextRand ( ) {

    double nextRandomValue = 0;

    for ( unsigned int i = 0; i < _distributions.size ( ); ++i ) {
        nextRandomValue += _distributions[ i ]->nextRand ( );
    }
    return nextRandomValue;

}
