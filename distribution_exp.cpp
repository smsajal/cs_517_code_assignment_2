#include "distribution_exp.h"
#include "random_helper.h"
#include "factory.h"


//my imports
#include <math.h>
#include <iostream>

REGISTER_CLASS(Distribution, DistributionExp)

//TODO fill in
// IMPORTANT: you may not use std::exponential_distribution or any other library for generating an exponential distribution
// Instead, use the uniform01 function in random_helper.h

DistributionExp::DistributionExp(Json::Value &config) : Distribution(config) {

    distributionParameter = config["rate"].asDouble();

}

DistributionExp::~DistributionExp() {

}

double DistributionExp::nextRand() {

    //std::cout<<"Inside DistributionExp::nextRand...\n";

    double uniformRandomValue = uniform01();


    double exponentialRandomValue = log(1 - uniformRandomValue) / ((-1) * distributionParameter);

    return exponentialRandomValue;

}