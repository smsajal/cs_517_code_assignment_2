#include "request_stream_open.h"
#include "factory.h"

#include "request_generator_dist.h"
#include "arrival_process_dist.h"

REGISTER_CLASS( RequestStream, RequestStreamOpen )

//TODO fill in


RequestStreamOpen::RequestStreamOpen ( Json::Value &config )
        : RequestStream ( config ),
          _requestGenerator ( Factory<RequestGenerator>::create ( config[ "requestGeneratorConfig" ] )),
          _arrivalProcess ( Factory<ArrivalProcess>::create ( config[ "arrivalProcessConfig" ] )) {



}

RequestStreamOpen::~RequestStreamOpen ( ) {

}

void RequestStreamOpen::init ( ) {

    // generate the first event in the queue....
    addArrivalEvent ();

}

Request *RequestStreamOpen::next ( ) {


    uint64_t currentTime = simulator::getSimTime ( );

    uint64_t nextArrivalTime = _arrivalProcess->nextArrival ( currentTime );

    Request *nextRequest = _requestGenerator->nextRequest ( nextArrivalTime );



    return nextRequest;
}

void RequestStreamOpen::notifyStart ( Request *req ) {

    RequestStream::notifyStart (req);
    addArrivalEvent ();

}

