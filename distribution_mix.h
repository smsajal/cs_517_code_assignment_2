#ifndef DISTRIBUTION_MIX_H
#define DISTRIBUTION_MIX_H

#include "json/json.h"
#include "distribution.h"

#include <vector>

class DistributionMix : public Distribution {
private:
    //TODO fill in
    Json::Value _config;
    std::vector<Distribution *> _distributions;
    std::vector<double> _probabilities;

    std::vector<double> _cumulativeProbabilities;

public:
    DistributionMix ( Json::Value &config );

    virtual ~DistributionMix ( );

    // Returns next random number
    virtual double nextRand ( );

    virtual void init ( );

    virtual std::vector<double> getCumulativeProbabilities ( );

    virtual int getIndexOfProbability ( double selectionProbability );
};

#endif /* DISTRIBUTION_MIX_H */
